# N9 - Desafio Final

    Todas as classes deverão estar em um mesmo arquivo main.py

Vamos construir uma relação de classes entre Moradia e Apartamento, para isso, siga as instruções a seguir.

Você deve criar 2 classes:

- Moradia
- Apartamento, que herda da classe Moradia

## **_Classe Moradia_**

> Classe que representa a descriçao básica de uma **_Moradia_** e **_um_** método para uso.

- ### _Atributos de instância :_

  - _`area`_
  - _`endereco`_
  - _`numero`_
  - _`preco_imovel`_
  - _`tipo`_

  > **_Atenção:_** _tipo_ deverá ser um atributo opcional com o valor padrão **'Não especificado'**

  ```python
  # Exemplo de chamada
  moradia_1 = Moradia(100, 'Rua das Limeiras', 672, 250000)

  print(moradia_1.__dict__)
  > {'area': 100, 'endereco': 'Rua das Limeiras', 'numero': 672, 'preco_imovel': 250000, 'tipo': 'Não especificado'}
  ```

- ### _Método de instância :_

  - _`gerar_relatorio_preco`_

    O método **_gerar_relatorio_preco_** realiza o cálculo de quanto custa o metro quadrado do imóvel, tendo em conta o seu preço e área e retorna o valor em **inteiro**

    ```python
    # Exemplo de saída utilizado a instancia criada no exemplo anterior:
    print(moradia_1.gerar_relatorio_preco())
    > 2500
    ```

## **_Classe Apartamento_**

> Classe que representa a descriçao básica de um **_Apartamento_** e **_um_** método para uso. Ela **herda** de **Moradia**

- ### _Atributo de classe :_

  - _`tipo`_

  > **_Atenção:_** **_tipo_** deverá ser inicializado com 'Apartamento'.

- ### _Atributo de instancia :_

  - _`preco_condominio`_
  - _`num_apt`_
  - _`num_elevadores`_
  - _`andar`_

  > **_Atenção:_** **_andar_** deverá ser inicializado a partir do **_num_apt_**, levando em conta que um apartamento numero 51 está no 5º andar, e um 101 está no 10º andar

  ```python
  # Exemplo de chamada
  apt_1 = Apartamento(100, 'Rua das Limeiras', 672, 500000, 700, 110, 2)

  print(apt_1.__dict__)
  > {'area': 100, 'endereco': 'Rua das Limeiras', 'numero': 672, 'preco_imovel': 500000, 'tipo': 'Apartamento', 'preco_condominio': 700, 'andar': 11, 'num_apt': 110, 'num_elevadores': 2}
  ```

- ### _Método de instância :_

  - _`gerar_relatorio`_

    O método **_gerar_relatorio_** será responsável por retornar uma **string** no seguinte formato:

    ```python
    # Exemplo de saída utilizado a instancia criada no exemplo anterior:
    print(apt_1.gerar_relatorio())
    > 'Rua das Limeiras - apt 110 - andar: 11 - elevadores: 2 - preco por m²: R$ 5000'
    ```

<br>
<br>

## Boa sorte Dev ! 👻
